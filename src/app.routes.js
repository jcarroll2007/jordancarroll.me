(function () {
    'use strict';

    function config($stateProvider, $urlRouterProvider) {

        ///////////////////////////
        // Redirects and Otherwise
        ///////////////////////////

        $urlRouterProvider
            .otherwise('/');

        ///////////////////////////
        // State Configurations
        ///////////////////////////

        $stateProvider
            .state('app', {
                abstract: true,
                templateUrl: 'app/landing/landing.html'
            })

            ///////////////////////////
            // Home
            ///////////////////////////
            .state('app.home', {
                url: '/',
                templateUrl: 'app/home/home.html'
            })

            ///////////////////////////
            // About
            ///////////////////////////
            .state('app.about', {
                url: 'about',
                template: '<about></about>'
            })

            ///////////////////////////
            // Contact
            ///////////////////////////
            .state('app.contact', {
                url: 'contact',
                template: '<contact></contact>'
            });
    }

    angular.module('app.routes', [])
        .config(config);
}());