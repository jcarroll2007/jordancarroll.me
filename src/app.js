(function () {
    'use strict';

    angular.module('jordancarroll', [
        'ui.router',
        'templates',
        'app.about',
        'app.routes',
        'app.common',
        'app.menu',
        'app.footer',
        'app.contact'
    ]);

}());