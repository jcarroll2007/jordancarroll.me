(function () {
    'use strict';

    function about() {
        return {
            restrict: 'E',
            templateUrl: 'app/about/about.html'
        };
    }

    angular.module('app.about', [])
        .directive('about', about);
}());