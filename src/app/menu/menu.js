(function () {
    'use strict';

    /*****************
     * Menu directive
     **/
    function menu() {
        return {
            templateUrl: 'app/menu/menu.html',
            restrict: 'E',
            replace: true
        };
    }

    angular.module('app.menu', [])
        .directive('menu', menu);
}());