/*global console, window*/
(function () {
    'use strict';

    function contact() {
        return {
            restrict: 'E',
            templateUrl: 'app/contact/contact.html'
        };
    }

    angular.module('app.contact', [])
        .directive('contact', contact);
}());