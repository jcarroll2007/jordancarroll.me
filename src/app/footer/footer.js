(function () {
    'use strict';

    function footer() {
        return {
            restrict: 'E',
            templateUrl: 'app/footer/footer.html',
            replace: true
        };
    }

    angular.module('app.footer', [])
        .directive('footer', footer);
}());