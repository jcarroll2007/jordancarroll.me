(function () {
    'use strict';

    function socialBanner() {
        return {
            restrict: 'E',
            templateUrl: 'common/social/banner/banner.html'
        };
    }

    angular.module('common.social.banner', [])
        .directive('socialBanner', socialBanner);
}());